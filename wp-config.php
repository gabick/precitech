<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'precitech_database');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'w^>^OGfUskc:g%DGsiNP[[C|+@r,<m5>Vy1=,sD}^{:j |OVY2x43tPfV$ah.Pfm');
define('SECURE_AUTH_KEY',  ')!8>pioCh?t&*8@0I_C [5Xy;1qR0:ri`d},Mrg8ume*df;B$hZ:9Ge$wxrr`Lqv');
define('LOGGED_IN_KEY',    'TfL6d%X+XRl@@t8yYu-L~Ib5HgpsuA<g_~$]^vn2p-M1!{QK h2|X68zCJ%F/T/p');
define('NONCE_KEY',        'McVmMnEN95l=2U5_Rc.C& t)G,R3N)Zsb+O]q{FCznw=;D;QSqg1&hNN05U=-s@3');
define('AUTH_SALT',        '3P(_bF=duUf~Cxm^/u_NXO{[N2k{c^+/il|3A07^<+eYGF#6gWbQ5}SuK.BCV0q4');
define('SECURE_AUTH_SALT', '&eU95QND!T(L#xaE`#CI4eNMi{93Zjn!osFsK0@a$Q<q6$dV,~st3Xd;@<c2W@xR');
define('LOGGED_IN_SALT',   ';ORX@&-VZeH6Bqd`h^zntN$3Z;,JvfRfwx3j0.F-NyXXZ0|/<jbR/6lMW&ZI!NLv');
define('NONCE_SALT',       'v@;9(3C:No&vK6Gqk!tE_u VgN@X#%97o&:^qlMkulyg7rhm}M(<p+5b5eXX~-@x');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
