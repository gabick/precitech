<?php
/**
 * Enqueue theme styles (parent first, child second)
 *
 */
function wpse218610_theme_styles() {

    $parent_style = 'divi';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'divi-child', get_stylesheet_directory_uri() . '/custom-styles.css', array( $parent_style ) );
}
add_action( 'wp_enqueue_scripts', 'wpse218610_theme_styles' );