<?php

function accueilMachineriesShortcode()
{
    global $post;

    $html = "";

    $my_query = new WP_Query(array(
        'post_type' => 'machineries',
        'posts_per_page' => 12
    ));
    if ($my_query->have_posts()) :
        $html .= "<div class='flex-container'>";
        while ($my_query->have_posts()) : $my_query->the_post();
            $image = get_the_post_thumbnail($post->ID);
            $shortDescription = get_the_excerpt();
            $descriptionTechniques = get_field('specifications_techniques');

            $html .= "<div class='items'>";
            $html .= '<div class="items-image">';
            $html .=    $image;
            $html .= '</div>';
            $html .= "<div class='item-container'>";
            $html .= "<h3 class='item-title'>" . get_the_title() . " </h3>";
            $html .= "<p class='item-text'>" . get_the_excerpt() . " </p>";
            $html .= "</div>"; // item-container
            $html .= "</div>"; // item
        endwhile;
        wp_reset_postdata();
        $html .= "</div>";
    endif;

    return $html;
}

function register_shortcodes()
{
    add_shortcode('accueilMachineries', 'accueilMachineriesShortcode');
}

add_action('init', 'register_shortcodes');

